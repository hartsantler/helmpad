#!/bin/bash

rm standalone/JuceLibraryCode/*.o

rm src/standalone/*.o
rm src/common/*.o
rm src/editor_components/*.o
rm src/editor_sections/*.o
rm src/look_and_feel/*.o
rm src/plugin/*.o
rm src/synthesis/*.o

rm mopo/src/*.o


