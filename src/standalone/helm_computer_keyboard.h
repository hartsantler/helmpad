/* Copyright 2013-2017 Matt Tytel
 *
 * helm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * helm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with helm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HELM_COMPUTER_KEYBOARD_H
#define HELM_COMPUTER_KEYBOARD_H

#include "JuceHeader.h"

#include "helm_common.h"
#include "helm_engine.h"

class HelmEditor;

class HelmComputerKeyboard : public mopo::StringLayout, public juce::KeyListener {
  public:
    HelmComputerKeyboard(mopo::HelmEngine* synth, 
                        juce::MidiKeyboardState* keyboard_state, 
                        HelmEditor* editor, bool use_pipe);
    ~HelmComputerKeyboard();

    void changeKeyboardOffset(int new_offset);

    // KeyListener
    bool keyPressed(const juce::KeyPress &key, juce::Component *origin) override;
    bool keyStateChanged(bool isKeyDown, juce::Component *origin) override;

    void noteOn(int note, float vel=1.0);
    void noteOff(int note);


  private:
    bool use_pipe_;
    bool autonext;
    HelmEditor* editor_;
    mopo::HelmEngine* synth_;
    juce::MidiKeyboardState* keyboard_state_;
    std::set<char> keys_pressed_;
    int computer_keyboard_offset_;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(HelmComputerKeyboard)
};

#endif  // HELM_COMPUTER_KEYBOARD_H
