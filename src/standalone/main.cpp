/* Copyright 2013-2017 Matt Tytel
 *
 * helm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * helm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with helm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "JuceHeader.h"
#include "border_bounds_constrainer.h"
#include "helm_editor.h"
#include "load_save.h"
#include "subprocess.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_joystick.h>
#include <SDL2/SDL_hints.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/shape.h>
#include <X11/Xatom.h>

#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xfixes.h>

#include <cairo.h>
#include <cairo-xlib.h>

#include "open_gl_oscilloscope.h"

Display *d;
Window overlay;
Window root;
int width, height;
cairo_t *cr = NULL;

void allow_input_passthrough (Window w) {
	XserverRegion region = XFixesCreateRegion (d, NULL, 0);

	XFixesSetWindowShapeRegion (d, w, ShapeBounding, 0, 0, 0);
	XFixesSetWindowShapeRegion (d, w, ShapeInput, 0, 0, region);

	XFixesDestroyRegion (d, region);
}

void prep_overlay (void) {
	overlay = XCompositeGetOverlayWindow (d, root);
	allow_input_passthrough (overlay);
}

//unsigned char plotdata[16000000];  // all black

void draw_overlay(cairo_t *cr, float x1, float y1) {
	//std::cout << "draw..." << std::endl;
	int plotheight;
	int plotstride;
	//int quarter_w = width / 2;
	//int quarter_w = width / 4;
	int quarter_w = width * ((int)x1+0.75 );
	// note if quarter_w becomes greater than width, then glitch bug is broken
	if (quarter_w > width)
		quarter_w = width;
	else if (quarter_w < 0)
		quarter_w = width / 4;

	int quarter_h = height / 4;


	int plotwidth = quarter_w;
	float* vec = getOutputMemory();

	cairo_set_source_rgb(cr, vec[0], vec[1], vec[2]);
	cairo_rectangle(cr, width/2, quarter_h, quarter_w * 2, quarter_h * 2);
	cairo_fill(cr);


	unsigned char *plotdata = (unsigned char*)&vec[ ((int)(y1+1))*128 ];

	if (x1 < 0.0) {
		plotheight = quarter_h;
	} else {
		plotheight = height;
	}



	if (x1 < -0.5) {
		cairo_set_source_rgb(cr, vec[3], vec[4], vec[5]);
		cairo_rectangle(cr, 0,0, width, height);
		cairo_fill(cr);


		plotstride = cairo_format_stride_for_width(CAIRO_FORMAT_A1,plotwidth);
		auto crsurfplot = cairo_image_surface_create_for_data(
			(unsigned char*)plotdata, 
			CAIRO_FORMAT_A1,
			plotwidth,plotheight,plotstride);
		cairo_set_source_surface(cr,crsurfplot,0,0);
		cairo_paint(cr);
		cairo_surface_destroy(crsurfplot);

	} else if (x1 <= 0.0) {
		cairo_set_source_rgb(cr, vec[6], vec[7], vec[8]);
		cairo_rectangle(cr, 0,height/2, width, height/2);
		cairo_fill(cr);


		plotstride = cairo_format_stride_for_width(CAIRO_FORMAT_A8,plotwidth);
		auto crsurfplot = cairo_image_surface_create_for_data(
			(unsigned char*)plotdata, 
			CAIRO_FORMAT_A8,
			plotwidth,plotheight,plotstride);
		cairo_set_source_surface(cr,crsurfplot,0,0);
		cairo_paint(cr);
		cairo_surface_destroy(crsurfplot);
		
	} else if (x1 < 0.5) {
		cairo_set_source_rgb(cr, vec[9], vec[10], vec[11]);
		cairo_rectangle(cr, 0,0, width, height/2);
		cairo_fill(cr);

		plotstride = cairo_format_stride_for_width(CAIRO_FORMAT_RGB16_565,plotwidth);
		auto crsurfplot = cairo_image_surface_create_for_data(
			(unsigned char*)plotdata, 
			CAIRO_FORMAT_RGB16_565,
			plotwidth,plotheight,plotstride);
		cairo_set_source_surface(cr,crsurfplot,0,0);
		cairo_paint(cr);
		cairo_surface_destroy(crsurfplot);
	
	} else {
		plotstride = cairo_format_stride_for_width(CAIRO_FORMAT_RGB24,plotwidth);
		auto crsurfplot = cairo_image_surface_create_for_data(
			(unsigned char*)plotdata, 
			CAIRO_FORMAT_RGB24,
			//CAIRO_FORMAT_ARGB32,
			plotwidth,plotheight,plotstride);
		cairo_set_source_surface(cr,crsurfplot,(int)x1*800, 0);
		cairo_paint(cr);
		cairo_surface_destroy(crsurfplot);
	}

	//auto plotstride = cairo_format_stride_for_width(CAIRO_FORMAT_RGB24,plotwidth);
	//auto plotstride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32,plotwidth);
	//auto plotdata = malloc(16000000);
	//void* plotdata = (void*)1024;
	//unsigned char plotdata[8192];


	//std::cout << "got output mem OK" << std::endl;
	//std::cout << plotdata << std::endl;
	//std::cout << "print mem OK" << std::endl;

/* this kills the 4096 display glitch - and shows no text
	cairo_set_source_rgb(cr, 1.0, 0.1, 0.1);
	cairo_select_font_face(cr, "Purisa", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 7);
	cairo_move_to(cr, 400, 200);
	//cairo_show_text(cr, (const char*)plotdata);

	std::string s = "";
	for (int i=0; i<4096*3; i++) {
		//std::cout << plotdata[i];
		s += (char)plotdata[i];
	}
	//std::cout << std::endl;
	cairo_show_text(cr, s.c_str());
*/

//XmbDrawString(d, osd->mask_bitmap, osd->fontset, osd->mask_gc, 300, 300, (const char*)plotdata, 4096);

	//if (plotdata) {
	//std::cout << "PAINTING MEM" << std::endl;
	//}
	//}
	//free( plotdata );
	//std::cout << "draw...OK" << std::endl;




}

extern unsigned long int HelmpadXID;  // defined in juce_gui_basics/native/juce_linux_X11_Windowing.cpp

static int gamepad1Buttons[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};
static int gamepad2Buttons[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};
static int gamepad3Buttons[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};
static int gamepad_offset = 0;
static bool use_pipe = false;
static bool print_ascii_video = false;

static subprocess::Popen *mplayer_subproc1 = NULL;
static char mplayer_out_buf1[4096*16];

static subprocess::Popen *mplayer_subproc2 = NULL;
static char mplayer_out_buf2[4096*16];

extern void update_helm_background_text_left(std::string s);
extern void update_helm_background_text_right(std::string s);
extern void redraw_helm_background_text();

class MplayerAsciiReader: private juce::Timer { 
	public:
	MplayerAsciiReader() {
		startTimer(33);
	};

	void timerCallback() override {
		if (mplayer_subproc1) {
			auto fp = mplayer_subproc1->output();
			int fd = fileno(fp);
			int read_bytes = read(fd, mplayer_out_buf1, sizeof(mplayer_out_buf1));
			//std::cout << "read bytes: " << read_bytes << std::endl;
			if (read_bytes != -1) {
				std::string left = std::string(mplayer_out_buf1, read_bytes);
				if (print_ascii_video)
					std::cout << left << std::endl;
				update_helm_background_text_left(left);
			}
		}
		if (mplayer_subproc2) {
			auto fp = mplayer_subproc2->output();
			int fd = fileno(fp);
			int read_bytes = read(fd, mplayer_out_buf2, sizeof(mplayer_out_buf2));
			if (read_bytes != -1) {
				std::string right = std::string(mplayer_out_buf2, read_bytes);
				update_helm_background_text_right(right);
			}
		}
		redraw_helm_background_text();
	};
};

static MplayerAsciiReader *mplayer_ascii_reader = NULL;

static std::string string_replace(std::string input, std::string tag, std::string target) {
	size_t pos = 0;
	std::string str = input;
	while((pos = str.find(tag, pos)) != std::string::npos) {
		str.replace(pos, tag.length(), target);
		pos += target.length();
	}
	return str;
}

void helm_mplayer_play(std::string fname, std::string side) {
	if (side=="left") {
		if (not mplayer_subproc1) {
			//std::cout << "HelmpadXID = " << HelmpadXID << std::endl;
			mplayer_subproc1 = new subprocess::Popen(
				// drawing into the helm pad window will not work :( because juce redraws too quickly? when the window is resized parts of the video are shown
				//{"mplayer", "-vo", "gl2", "-x", "1280", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-wid", std::to_string(HelmpadXID).c_str(), fname.c_str(), "-framedrop", "-nocorrect-pts", "-quiet"}, 
				//{"mplayer", "-vo", "caca", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-wid", std::to_string(HelmpadXID).c_str(), fname.c_str(), "-framedrop", "-nocorrect-pts", "-quiet"}, 
				//{"mplayer", "-vo", "matrixview", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", fname.c_str(), "-framedrop", "-nocorrect-pts", "-quiet"}, 
				//-into xid may work with the right settings {"xterm", "-into", std::to_string(HelmpadXID).c_str(), "-fn", "5x7", "-geometry", "250x180", "-e", "mplayer", "-quiet", "-vo", "aa:driver=curses", fname.c_str()},
				// note: this fails to response to gamepad input, because its piped to xterm and note mplayer {"xterm", "-fn", "5x7", "-geometry", "250x180", "-e", "mplayer", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-quiet", "-vo", "aa:driver=curses", fname.c_str()},
				// available aa drivers:linux slang X11 stdout stderr curses

				{"mplayer", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-really-quiet", "-vo", "aa:driver=stdout:width=160:height=120", fname.c_str()},
				// these also run fast - but near the limit
				//{"mplayer", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-really-quiet", "-vo", "aa:driver=stdout:width=240:height=120", fname.c_str()},
				//{"mplayer", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-really-quiet", "-vo", "aa:driver=stdout:width=200:height=120", fname.c_str()},

				subprocess::input{subprocess::PIPE},
				subprocess::output{subprocess::PIPE}
			);
			if (not mplayer_ascii_reader)
				mplayer_ascii_reader = new MplayerAsciiReader();
		} else {
			fname = string_replace(fname, " ", "\\ ");
			fname = string_replace(fname, "[", "\\[");
			fname = string_replace(fname, "]", "\\]");
			std::string cmd = "loadfile ";
			cmd += fname;
			cmd += "\n";
			mplayer_subproc1->send(cmd.c_str(), cmd.size());
		}
	} else if (side=="right") {
		if (not mplayer_subproc2) {
			mplayer_subproc2 = new subprocess::Popen(
				{"mplayer", "-idle", "-loop", "10", "-nojoystick", "-nomouseinput", "-slave", "-really-quiet", "-vo", "aa:driver=stdout:width=160:height=120", fname.c_str()},
				subprocess::input{subprocess::PIPE},
				subprocess::output{subprocess::PIPE}
			);
			if (not mplayer_ascii_reader)
				mplayer_ascii_reader = new MplayerAsciiReader();
		} else {
			fname = string_replace(fname, " ", "\\ ");
			fname = string_replace(fname, "[", "\\[");
			fname = string_replace(fname, "]", "\\]");
			std::string cmd = "loadfile ";
			cmd += fname;
			cmd += "\n";
			mplayer_subproc2->send(cmd.c_str(), cmd.size());
		}

	}
}


class HelmGamepadManager: private juce::Timer { 
	public:
		HelmEditor* editor;
		SDL_Joystick *primary_joystick;
		SDL_Joystick *secondary_joystick;
		SDL_Joystick *third_joystick;
		SDL_Joystick *pad1;
		SDL_Joystick *pad2;
		SDL_Joystick *pad3;
		//SDL_Joystick *fourth_joystick;
		int gamepadHat1 = 0;
		int gamepadHat2 = 0;
		int gamepadHat3 = 0;
		// analog locks
		bool pad1analog1_locked = false;
		bool pad1analog2_locked = false;
		bool pad2analog1_locked = false;
		bool pad2analog2_locked = false;
		bool pad3analog1_locked = false;
		bool pad3analog2_locked = false;
		std::pair<float,float> pad1analog1 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad1analog2 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad2analog1 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad2analog2 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad3analog1 = std::pair<float,float>(0.0, 0.0);
		std::pair<float,float> pad3analog2 = std::pair<float,float>(0.0, 0.0);
		int prevNote = -1;
		bool analogKeyboard = false;
		int analogKeyboardOffset = 0;
		std::vector<std::map<std::string,std::pair<float,float>>> recording_data = std::vector< std::map<std::string,std::pair<float,float>> >();
		bool recording = false;
		bool playing = false;
		float playback_frame = -1.0;
		float playback_rate = 1.0;
		int gamepad_shift = -1;

		void shift_gamepads(int shift) {
			this->gamepad_shift += shift;
			if (this->gamepad_shift < 0)
				this->gamepad_shift = 0;

			if (this->pad3) {
				if (this->gamepad_shift >= 3)
					this->gamepad_shift = 2;

				switch (this->gamepad_shift) {
					case 0:
						this->primary_joystick = this->pad1;
						this->secondary_joystick = this->pad2;
						this->third_joystick = this->pad3;
						break;
					case 1:
						this->primary_joystick = this->pad3;
						this->secondary_joystick = this->pad1;
						this->third_joystick = this->pad2;
						break;
					case 2:
						this->primary_joystick = this->pad2;
						this->secondary_joystick = this->pad3;
						this->third_joystick = this->pad1;
						break;
				}        
			}
			else {
				if (this->gamepad_shift >= 2)
					this->gamepad_shift = 1;

				switch (this->gamepad_shift) {
					case 0:
						this->primary_joystick = this->pad1;
						this->secondary_joystick = this->pad2;
						break;
					case 1:
						this->primary_joystick = this->pad2;
						this->secondary_joystick = this->pad1;
						break;
				}
			}
		}

		HelmGamepadManager( HelmEditor* editor_, SDL_Joystick* p1, SDL_Joystick* p2, SDL_Joystick* p3) {
			std::cout << "HelmGamepadManager enter constructor..." << std::endl;
			std::cout << "	gamepad1 addr = " <<  p1 << std::endl;
			std::cout << "	gamepad2 addr = " <<  p2 << std::endl;
			std::cout << "	gamepad3 addr = " <<  p3 << std::endl;

			d = XOpenDisplay(NULL);
			int s = DefaultScreen(d);
			root = RootWindow(d, s);
			XCompositeRedirectSubwindows (d, root, CompositeRedirectAutomatic);
			XSelectInput (d, root, SubstructureNotifyMask);
			width = DisplayWidth(d, s);
			height = DisplayHeight(d, s);
			prep_overlay();
			cairo_surface_t *surf = cairo_xlib_surface_create(d, overlay,
										  DefaultVisual(d, s),
										  width, height);
			cr = cairo_create(surf);
			XSelectInput(d, overlay, ExposureMask);
			draw_overlay(cr, -1.0, -1.0);


			this->editor = editor_;
			this->primary_joystick = p1;
			this->secondary_joystick = p2;
			this->third_joystick = p3;
			this->pad1 = p1;
			this->pad2 = p2;
			this->pad3 = p3;

			if (this->primary_joystick) {
				this->editor->linkGamepadAxis(std::string("osc_feedback_amount"), 0);
				this->editor->linkGamepadAxis(std::string("cutoff"), 1);
				this->editor->linkGamepadAxis(std::string("portamento"), 2);
				this->editor->linkGamepadAxis(std::string("resonance"), 3);

				this->editor->linkGamepadButton(std::string("formant_on"), 4);
				this->editor->linkGamepadButton(std::string("delay_on"), 4);
				this->editor->linkGamepadButton(std::string("filter_on"), 7);
				//this->editor->linkGamepadButton(std::string("stutter_on"), 5);
				this->editor->linkGamepadButton(std::string("distortion_on"), 6);
				this->editor->linkGamepadButton(std::string("reverb_on"), 5);
				//this->editor->linkGamepadButton(std::string("arp_on"), 7);

				this->editor->linkGamepadAxis(std::string("formant_x"), 2);
				this->editor->linkGamepadAxis(std::string("formant_y"), 3);

			}
			if (this->secondary_joystick) {
				this->analogKeyboard = true;
				this->editor->linkGamepadAxis(std::string("osc_feedback_amount"), 1);
				this->editor->linkGamepadAxis(std::string("cutoff"), 2);
				this->editor->linkGamepadAxis(std::string("portamento"), 3);
				this->editor->linkGamepadAxis(std::string("resonance"), 3);

				this->editor->linkGamepadAxis(std::string("arp_gate"), 4);
				this->editor->linkGamepadAxis(std::string("sub_shuffle"), 4);
				this->editor->linkGamepadAxis(std::string("beats_per_minute"), 5);
				this->editor->linkGamepadAxis(std::string("osc_1_transpose"), 6);
				this->editor->linkGamepadAxis(std::string("osc_2_transpose"), 7);
			}
			if (this->third_joystick) {
				this->editor->linkGamepadAxis(std::string("osc_1_tune"), 8);
				//this->editor->linkGamepadAxis(std::string("osc_1_transpose"), 9);
				//this->editor->linkGamepadAxis(std::string("osc_1_volume"), 9);
				this->editor->linkGamepadAxis(std::string("delay_feedback"), 9);

				this->editor->linkGamepadAxis(std::string("osc_2_tune"), 10);
				//this->editor->linkGamepadAxis(std::string("osc_2_transpose"), 11);
				//this->editor->linkGamepadAxis(std::string("osc_2_volume"), 11);
				this->editor->linkGamepadAxis(std::string("cross_modulation"), 11);
			}
			//startTimer(60);  // do not call startTimer here
		}
		~HelmGamepadManager() {}

		void start() {
			startTimer(60);
		}

		void timerCallback() override {

			// secondary gamepad
			float x3 = 0.0;
			float y3 = 0.0;
			float x4 = 0.0;
			float y4 = 0.0;
			// 3rd gamepad
			float x5 = 0.0;
			float y5 = 0.0;
			float x6 = 0.0;
			float y6 = 0.0;

			int btns[12]  = {0,0,0,0, 0,0,0,0, 0,0,0,0};
			int btns2[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};
			int btns3[12] = {0,0,0,0, 0,0,0,0, 0,0,0,0};

			SDL_PumpEvents();  // poll event not required when calling pump

			int hat = SDL_JoystickGetHat(this->primary_joystick, 0);
			bool button_lock = hat == 1;  // if hat is up
			if (hat != this->gamepadHat1) {
				//std::cout << hat << std::endl;
				switch (hat) {
					case 1: // up
						//this->editor->noteOn( 64 );
						this->analogKeyboard=true;
						break;
					case 2: // right
						this->editor->nextPatch();
						break;
					case 4: // down
						this->analogKeyboard=false;
						break;
					case 8: // left
						this->editor->prevPatch();
						break;
				}
				//if (hat==0)
				//  emit gamepadHatReleased();
				//else
				//  emit gamepadHatPressed(hat);
			}
			this->gamepadHat1 = hat;

			float x1 = ((double)SDL_JoystickGetAxis(this->primary_joystick, 0)) / 32768.0;
			float y1 = ((double)SDL_JoystickGetAxis(this->primary_joystick, 1)) / 32768.0;
			//double z1 = (((double)SDL_JoystickGetAxis(m_joystick, 2)) / 32768.0) + 1.0;  // xbox gamepads have 6 axes
			float x2 = ((double)SDL_JoystickGetAxis(this->primary_joystick, 2)) / 32768.0;
			float y2 = ((double)SDL_JoystickGetAxis(this->primary_joystick, 3)) / 32768.0;
			//double z2 = (((double)SDL_JoystickGetAxis(m_joystick, 5)) / 32768.0) + 1.0;

			// update X overlay
			if (this->analogKeyboard) {
				//std::cout << "getting overlay window" << std::endl;
				overlay = XCompositeGetOverlayWindow(d, root);
				//std::cout << "about to draw..." << std::endl;
				draw_overlay(cr, x1, y1);
				//std::cout << "release overlay window" << std::endl;
				XCompositeReleaseOverlayWindow(d, root);
			}

			for (int i=0; i<12; i++) {
				btns[i] = 0;
				if (SDL_JoystickGetButton(this->primary_joystick, i)) {
					if (gamepad1Buttons[i]==0) {
						gamepad1Buttons[i] = 1;
						btns[i] = 1;
						//if (use_pipe)
						//	std::cout << "{\"button\":" << i << ",\"value\":1}" << std::endl;
					} else {
						gamepad1Buttons[i] += 1;
						btns[i] = gamepad1Buttons[i];
					}
				} else {
					if (gamepad1Buttons[i] >= 1) {
						gamepad1Buttons[i] = 0;
						btns[i] = -1;
						//if (use_pipe)
						//	std::cout << "{\"button\":" << i << ",\"value\":-1}" << std::endl;
					}
				}
			}

/*
			if (btns[8]==1)  // select button
				this->pad1analog1_locked = false;
			else if (btns[10] == 1){  // left analog button
				this->pad1analog1_locked = true;
				this->pad1analog1.first = x1;
				this->pad1analog1.second = y1;
			}
			if (btns[9]==1)  // start button
				this->pad1analog2_locked = false;
			else if (btns[11] == 1){  // right analog button
				this->pad1analog2_locked = true;
				this->pad1analog2.first = x2;
				this->pad1analog2.second = y2;
			}
			if (this->pad1analog1_locked){
				x1 = this->pad1analog1.first;
				y1 = this->pad1analog2.second;
			}
			if (this->pad1analog2_locked){
				x2 = this->pad1analog2.first;
				y2 = this->pad1analog2.second;
			}
*/

			if (this->secondary_joystick) {
				x3 = ((double)SDL_JoystickGetAxis(this->secondary_joystick, 0)) / 32768.0;
				y3 = ((double)SDL_JoystickGetAxis(this->secondary_joystick, 1)) / 32768.0;
				x4 = ((double)SDL_JoystickGetAxis(this->secondary_joystick, 2)) / 32768.0;
				y4 = ((double)SDL_JoystickGetAxis(this->secondary_joystick, 3)) / 32768.0;

				for (int i=0; i<12; i++) {
					btns2[i] = 0;
					if (SDL_JoystickGetButton(this->secondary_joystick, i)) {
						if (gamepad2Buttons[i]==0) {
							gamepad2Buttons[i] = 1;
							btns2[i] = 1;
						} else {
							gamepad2Buttons[i] += 1;
							btns2[i] = gamepad2Buttons[i];
						}
					} else {
						if (gamepad2Buttons[i] >= 1) {
							gamepad2Buttons[i] = 0;
							btns2[i] = -1;
						}
					}
				}

/*
				if (btns2[8]==1)  // select button
					this->pad2analog1_locked = false;
				else if (btns2[10] == 1){  // left analog button
					this->pad2analog1_locked = true;
					this->pad2analog1.first = x3;
					this->pad2analog1.second = y3;
				}
				if (btns2[9]==1)  // start button
					this->pad2analog2_locked = false;
				else if (btns2[11] == 1){  // right analog button
					this->pad2analog2_locked = true;
					this->pad2analog2.first = x4;
					this->pad2analog2.second = y4;
				}
				if (this->pad2analog1_locked){
					x3 = this->pad2analog1.first;
					y3 = this->pad2analog2.second;
				}
				if (this->pad2analog2_locked){
					x4 = this->pad2analog2.first;
					y4 = this->pad2analog2.second;
				}
*/

				int hat2 = SDL_JoystickGetHat(this->secondary_joystick, 0);
				if (hat2 != this->gamepadHat2) {
					switch (hat2) {
						case 1: // up
							this->shift_gamepads(-1);
							break;
						case 4: // down
							this->shift_gamepads(1);
							break;
						case 2: // right
							this->editor->nextPatch();
							break;
						case 8: // left
							this->editor->prevPatch();
							break;
					}
				}
				this->gamepadHat2 = hat2;
			}

			if (mplayer_subproc1) {
				std::string cmd = "speed_set ";
				cmd += std::to_string( (x1+1) + (y1*0.25));
				cmd += "\n";  // required to send a new line at the end of the command
				mplayer_subproc1->send(cmd.c_str(), cmd.size());
			}
			if (mplayer_subproc2) {
				std::string cmd = "speed_set ";
				cmd += std::to_string( (x2+1) + (y2*0.25));
				cmd += "\n";  // required to send a new line at the end of the command
				mplayer_subproc2->send(cmd.c_str(), cmd.size());
			}

			if (this->third_joystick) {
				x5 = ((double)SDL_JoystickGetAxis(this->third_joystick, 0)) / 32768.0;
				y5 = ((double)SDL_JoystickGetAxis(this->third_joystick, 1)) / 32768.0;
				x6 = ((double)SDL_JoystickGetAxis(this->third_joystick, 2)) / 32768.0;
				y6 = ((double)SDL_JoystickGetAxis(this->third_joystick, 3)) / 32768.0;

				for (int i=0; i<12; i++) {
					btns3[i] = 0;
					if (SDL_JoystickGetButton(this->third_joystick, i)) {
						if (gamepad3Buttons[i]==0) {
							gamepad3Buttons[i] = 1;
							btns3[i] = 1;
						} else {
							gamepad3Buttons[i] += 1;
							btns3[i] = gamepad3Buttons[i];
						}
					} else {
						if (gamepad3Buttons[i] >= 1) {
							gamepad3Buttons[i] = 0;
							btns3[i] = -1;
						}
					}
				}

/*
				if (btns3[8]==1)  // select button
					this->pad3analog1_locked = false;
				else if (btns3[10] == 1){  // left analog button
					this->pad3analog1_locked = true;
					this->pad3analog1.first = x5;
					this->pad3analog1.second = y5;
				}
				if (btns3[9]==1)  // start button
					this->pad3analog2_locked = false;
				else if (btns2[11] == 1){  // right analog button
					this->pad3analog2_locked = true;
					this->pad3analog2.first = x6;
					this->pad3analog2.second = y6;
				}
				if (this->pad3analog1_locked){
					x5 = this->pad3analog1.first;
					y5 = this->pad3analog2.second;
				}
				if (this->pad3analog2_locked){
					x6 = this->pad3analog2.first;
					y6 = this->pad3analog2.second;
				}
*/

				int hat3 = SDL_JoystickGetHat(this->third_joystick, 0);
				if (hat3 != this->gamepadHat3) {
					switch (hat3) {
						case 1: // up
							this->shift_gamepads(-1);
							break;
						case 4: // down
							this->shift_gamepads(1);
							break;
						case 2: // right
							this->editor->nextPatch();
							break;
						case 8: // left
							this->editor->prevPatch();
							break;
					}
				}
				this->gamepadHat3 = hat3;
			}


			if (this->analogKeyboard) {
				float x2mult = 8.0;

				if (btns[4] >= 1){
					//this->analogKeyboardOffset = static_cast<int>( (x2+0.5) * 40);
					x2mult = 0.0;
				}
				else if (btns[5] >= 1) {
					x2mult *= 0.3;
				}
				if (btns[6] >= 1) x2mult *= 2;
				if (btns[7] >= 1) x2mult *= 3;

				/*
				else if (btns[8]==1)
							this->analogKeyboardOffset -= 1;
				else if (btns[9]==1)
							this->analogKeyboardOffset += 1;
				*/

				//int keyindex = static_cast<int>( (x1 + x2) * 64.0 );
				//int keyindex = static_cast<int>( ((x1+0.5)*64.0) + (x2*32.0) );
				int keyindex = static_cast<int>( ((x1+1.0)*64.0) + (x2*x2mult) );
				keyindex += this->analogKeyboardOffset;
				if (keyindex != this->prevNote) {
					if (this->prevNote != -1)
						this->editor->noteOff( this->prevNote );
					this->editor->noteOn( keyindex, -y2+1.1 );
					this->prevNote = keyindex;
				}
			}

			if (use_pipe) {
				std::cout << "{"
				<< "\"pad1\":["
					<< x1 << "," << y1 << "," << x2 << "," << y2 << "]" << ","
				<< "\"pad2\":["
					<< x3 << "," << y3 << "," << x4 << "," << y4 << "]" << ","
				<< "\"pad3\":["
					<< x5 << "," << y5 << "," << x6 << "," << y6 << "]" << ","
				<< "\"buttons\":["
					<< btns[0] << "," << btns[1] << "," << btns[2] << "," << btns[3] << "," << btns[4] << "," << btns[5] << "," << btns[6] << "," << btns[7] << "," << btns[8] << "," << btns[9] << "," << btns[10] << "," << btns[11] << "]"

				<< "}" << std::endl;
			}


			// updates slider buttons set by user from UI
			this->editor->updateGamepad(
				x1,y1, x2,y2, // primary gamepad
				x3,y3, x4,y4, // optional secondary gamepad
				x5,y5, x6,y6, // optional 3rd gamepad
				btns[0], 
				btns[1], 
				btns[2], 
				btns[3], 
				btns[4], 
				btns[5], 
				btns[6], 
				btns[7], 
				btns[8], 
				btns[9], 
				btns[10], 
				btns[11],
				button_lock
			);
		}
};

static HelmGamepadManager *s_gamepad_manager = NULL;
static HelmEditor *s_helm_editor = NULL;

class HelmApplication : public juce::JUCEApplication {
	public:
		class MainWindow : public juce::DocumentWindow, public juce::ApplicationCommandTarget, public juce::FileDragAndDropTarget, private juce::AsyncUpdater {
		public:
			enum PatchCommands {
				kSave = 0x5001,
				kSaveAs,
				kOpen,
			};

			MainWindow(juce::String name, bool visible = true) :
					DocumentWindow(name, juce::Colour(0x11000000)/*Colours::lightgrey*/, juce::DocumentWindow::allButtons, visible) {
				editor_ = new HelmEditor(visible, use_pipe);

				// fixes window managers that do not adjust a windows start position to take into account the titlebar header height
				setBounds(32,64, 1280, 500);

				s_helm_editor = editor_;
				if (visible) {
					//editor_->animate(LoadSave::shouldAnimateWidgets());
					editor_->animate(true);

					setUsingNativeTitleBar(true);
					setContentOwned(editor_, true);
					setResizable(true, true);
					/*
					constrainer_.setMinimumSize(2 * mopo::DEFAULT_WINDOW_WIDTH / 3,
																			2 * mopo::DEFAULT_WINDOW_HEIGHT / 3);
					constrainer_.setBorder(getPeer()->getFrameSize());
					double ratio = (1.0 * mopo::DEFAULT_WINDOW_WIDTH) / mopo::DEFAULT_WINDOW_HEIGHT;

					constrainer_.setFixedAspectRatio(ratio);
					setConstrainer(&constrainer_);
					*/
					//centreWithSize(getWidth(), getHeight());
					setVisible(visible);
					triggerAsyncUpdate();
				}
				else
					editor_->animate(false);
			}

			void closeButtonPressed() override {
				juce::JUCEApplication::getInstance()->systemRequestedQuit();
			}

			bool loadFile(juce::File file) {
				bool success = editor_->loadFromFile(file);
				if (success)
					editor_->externalPatchLoaded(file);
				return success;
			}

			bool isInterestedInFileDrag (const juce::StringArray& /*files*/) {
				// normally you'd check these files to see if they're something that you're
				// interested in before returning true, but for the demo, we'll say yes to anything..
				return true;
			}

			void filesDropped (const juce::StringArray& files, int x, int y) {
				//auto message = "files dropped: " + files.joinIntoString("\n");
				std::string fname = files[0].toStdString();
				std::cout << fname << std::endl;
				if (x < 650) {
					helm_mplayer_play(fname, "left");
				} else {
					helm_mplayer_play(fname, "right");					
				}
			}

			juce::ApplicationCommandTarget* getNextCommandTarget() override {
				return findFirstTargetParentComponent();
			}

			void getAllCommands(juce::Array<juce::CommandID>& commands) override {
				commands.add(kSave);
				commands.add(kSaveAs);
				commands.add(kOpen);
			}

			void getCommandInfo(const juce::CommandID commandID, juce::ApplicationCommandInfo& result) override {
				/*
				if (commandID == kSave) {
					result.setInfo(TRANS("Save"), TRANS("Saves the current patch"), "Application", 0);
					result.defaultKeypresses.add( KeyPress('s', juce::ModifierKeys::commandModifier, 0));
				}
				else if (commandID == kSaveAs) {
					result.setInfo(TRANS("Save As"), TRANS("Saves patch to a new file"), "Application", 0);
					juce::ModifierKeys modifier = juce::ModifierKeys::commandModifier | juce::ModifierKeys::shiftModifier;
					result.defaultKeypresses.add( KeyPress('s', modifier, 0));
				}
				else if (commandID == kOpen) {
					result.setInfo(TRANS("Open"), TRANS("Opens a patch"), "Application", 0);
					result.defaultKeypresses.add( KeyPress('o', juce::ModifierKeys::commandModifier, 0));
				}
				*/
			}

			void open() {
				juce::File active_file = editor_->getActiveFile();
				juce::FileChooser open_box("Open Patch", juce::File(),
														 juce::String("*.") + mopo::PATCH_EXTENSION);
				if (open_box.browseForFileToOpen())
					loadFile(open_box.getResult());
			}

			bool perform(const InvocationInfo& info) override {
				if (info.commandID == kSave) {
					if (!editor_->saveToActiveFile())
						editor_->exportToFile();
					grabKeyboardFocus();
					editor_->setFocus();
					return true;
				}
				if (info.commandID == kSaveAs) {
					editor_->exportToFile();
					grabKeyboardFocus();
					editor_->setFocus();
					return true;
				}
				if (info.commandID == kOpen) {
					open();
					grabKeyboardFocus();
					editor_->setFocus();
					return true;
				}

				return false;
			}

			void handleAsyncUpdate() override {
				command_manager_ = new juce::ApplicationCommandManager();
				command_manager_->registerAllCommandsForTarget( juce::JUCEApplication::getInstance());
				command_manager_->registerAllCommandsForTarget(this);
				addKeyListener(command_manager_->getKeyMappings());
				editor_->setFocus();
			}

			void activeWindowStatusChanged() override {
				if (editor_)
					//editor_->animate(LoadSave::shouldAnimateWidgets() && isActiveWindow());
					editor_->animate(true);

				if (isActiveWindow())
					editor_->grabKeyboardFocus();
			}
			// this may help some window managers that do not auto force a new window to fit the screen
			//int getTitleBarHeight() const {
			//	return 64;
			//}


		private:
			JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainWindow)
			HelmEditor* editor_;
			juce::ScopedPointer<juce::ApplicationCommandManager> command_manager_;
			//BorderBoundsConstrainer constrainer_;
		};

		HelmApplication() { }

		const juce::String getApplicationName() override { return ProjectInfo::projectName; }
		const juce::String getApplicationVersion() override { return ProjectInfo::versionString; }
		bool moreThanOneInstanceAllowed() override { return true; }

		void init_sdl() {
			//SDL_Init(SDL_INIT_JOYSTICK);
			SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS,"1");  // required in SDL2 when initialized before display
			SDL_InitSubSystem(SDL_INIT_JOYSTICK);
			//SDL_JoystickEventState(SDL_ENABLE);

			// Check for joystick
			int num_gamepads = SDL_NumJoysticks();
			std::cout << "number of gamepads: " << num_gamepads << std::endl;

			if (num_gamepads > 0) {
				std::cout << "gamepads global offset: " << gamepad_offset << std::endl;
				// Open joystick
				auto joystick1 = SDL_JoystickOpen(gamepad_offset);
				if (joystick1) {
					std::cout <<"Primary Gamepad: " << gamepad_offset << std::endl;
					std::cout <<"Primary Name: " << SDL_JoystickNameForIndex(gamepad_offset) << std::endl;
					std::cout <<"Number of Axes: " << SDL_JoystickNumAxes(joystick1) << std::endl;
					std::cout <<"Number of Buttons: " << SDL_JoystickNumButtons(joystick1) << std::endl;
					std::cout <<"Number of Balls: " << SDL_JoystickNumBalls(joystick1) << std::endl;
					std::cout <<"Number of Hats: " << SDL_JoystickNumHats(joystick1) << std::endl;
				} else {
					std::cout << "ERROR: failed to open primary gamepad" << std::endl;
				}
				std::cout << "creating gamepad manager..." << std::endl;
				std::cout << "HelmEditor addr = " << s_helm_editor << std::endl;
				if (num_gamepads==1) {
					s_gamepad_manager = new HelmGamepadManager(s_helm_editor, joystick1, NULL, NULL);
				} else if (num_gamepads==2) {
					auto joystick2 = SDL_JoystickOpen(gamepad_offset + 1);
					s_gamepad_manager = new HelmGamepadManager(s_helm_editor, joystick1, joystick2, NULL);
				} else {
					auto joystick2 = SDL_JoystickOpen(gamepad_offset + 1);
					auto joystick3 = SDL_JoystickOpen(gamepad_offset + 2);
					s_gamepad_manager = new HelmGamepadManager(s_helm_editor, joystick1, joystick2, joystick3);
				}
			} else {
				std::cout << "No gamepads are attached" << std::endl;
			}
		}


		void initialise(const juce::String& command_line) override {
			juce::String command = " " + command_line + " ";
			if (command.contains(" --version ") || command.contains(" -v ")) {
				std::cout << getApplicationName() << " " << getApplicationVersion() << std::endl;
				quit();
			}
			else if (command.contains(" --help ") || command.contains(" -h ")) {
				std::cout << "Usage:" << std::endl;
				std::cout << "  " << getApplicationName().toLowerCase() << " [OPTION...]" << std::endl << std::endl;
				std::cout << getApplicationName() << " polyphonic, semi-modular synthesizer." << std::endl << std::endl;
				std::cout << "Help Options:" << std::endl;
				std::cout << "  -h, --help                          Show help options" << std::endl << std::endl;
				std::cout << "Application Options:" << std::endl;
				std::cout << "  -v, --version                       Show version information and exit" << std::endl;
				std::cout << "  --headless                          Run without graphical interface." << std::endl << std::endl;
				std::cout << "  --gamepad=n                         For use with multiple gamepads, the offset index." << std::endl << std::endl;
				std::cout << "  --pipe                              print events in json format to stdout" << std::endl << std::endl;
				std::cout << "  --print-ascii-video                 print mplayer ascii video frames to stdout" << std::endl << std::endl;
				quit();
			}
			if (command.contains(" --pipe ")) {
				use_pipe = true;
			}
			if (command.contains(" --print-ascii-video ")) {
				print_ascii_video = true;
			}
			if (command.contains(" --gamepad=2 ")) {
				gamepad_offset = 1;
			}
			else if (command.contains(" --gamepad=3 ")) {
				gamepad_offset = 2;
			}
			else if (command.contains(" --gamepad=4 ")) {
				gamepad_offset = 3;
			}
			else if (command.contains(" --gamepad=5 ")) {
				gamepad_offset = 4;
			}
			else if (command.contains(" --gamepad=6 ")) {
				gamepad_offset = 5;
			}

			bool visible = !command.contains(" --headless ");
			main_window_ = new MainWindow(getApplicationName(), visible);

			juce::StringArray args = getCommandLineParameterArray();
			juce::File file;

			for (int i = 0; i < args.size(); ++i) {
				if (args[i] != "" && args[i][0] != '-' && loadFromCommandLine(args[i]))
					return;
			}
			std::cout << "HelmApplication initialise OK" << std::endl;
			init_sdl();
			if (s_gamepad_manager) {
				std::cout << "HelmApplication starting gamepad timerCallback" << std::endl;
				s_gamepad_manager->start();
			}
		}


		bool loadFromCommandLine(const juce::String& command_line) {
			juce::String file_path = command_line;
			if (file_path[0] == '"' && file_path[file_path.length() - 1] == '"')
				file_path = command_line.substring(1, command_line.length() - 1);
			juce::File file = juce::File::getCurrentWorkingDirectory().getChildFile(file_path);
			if (file.exists())
				return main_window_->loadFile(file);
			return false;
		}

		void shutdown() override {
			main_window_ = nullptr;
		}

		void systemRequestedQuit() override {
			quit();
		}

		void anotherInstanceStarted(const juce::String& command_line) override {
			loadFromCommandLine(command_line);
		}

	private:
		juce::ScopedPointer<MainWindow> main_window_;
};

#ifdef EMBED_HELM
	juce::JUCEApplicationBase* juce_CreateHelmApplication() {
		std::cout << "enter juce_CreateHelmApplication..." << std::endl;
		return new HelmApplication(); 
	}

	extern "C" int helm_main(int argc, const char** argv) {
		std::cout << "enter helm_main..." << std::endl;
		juce::JUCEApplicationBase::createInstance = &juce_CreateHelmApplication;
		int err = juce::JUCEApplicationBase::main(argc, argv);
		std::cout << "helm_main exit code: " << err << std::endl;
		bool ensure_mplayer_dead = false;
		if (mplayer_subproc1) {
			ensure_mplayer_dead = true;
			std::string cmd = "exit\n";
			mplayer_subproc1->send(cmd.c_str(), cmd.size());
			delete mplayer_subproc1;
		}
		if (mplayer_subproc2) {
			ensure_mplayer_dead = true;
			std::string cmd = "exit\n";
			mplayer_subproc2->send(cmd.c_str(), cmd.size());
			delete mplayer_subproc2;
		}
		if (ensure_mplayer_dead) {
			subprocess::Popen({"killall", "mplayer"});  // ensure mplayer is really killed
			subprocess::Popen({"killall", "mplayer"});  // really ensure mplayer is killed
			std::cout << "\r" << std::endl;
		}
		return err;
	}

#else

	START_JUCE_APPLICATION(HelmApplication)

#endif
