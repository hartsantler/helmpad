/* Copyright 2013-2017 Matt Tytel
 *
 * helm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * helm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with helm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYNTH_BASE_H
#define SYNTH_BASE_H

#include "JuceHeader.h"
#include "concurrentqueue.h"

#include "helm_common.h"
#include "helm_engine.h"
#include "mopo_memory.h"
#include "midi_manager.h"
#include <string>

class SynthGuiInterface;

class SynthBase : public MidiManager::Listener {
  public:
    SynthBase();
    virtual ~SynthBase() { }

    // gamepad callbacks 
    void linkGamepadButton(const std::string& name, int index);
    void unlinkGamepadButton(const std::string& name);
    int getGamepadButtonLinkedTo(const std::string& name);

    void linkGamepadAxis(const std::string& name, int index);
    void unlinkGamepadAxis(const std::string& name);
    int getGamepadAxisLinkedTo(const std::string& name);

    void updateGamepad(
        float x1, float y1, float x2, float y2,  // first gamepad
        float x3, float y3, float x4, float y4,  // second gamepad
        float x5, float y5, float x6, float y6,  // third gamepad
        int b0,
        int b1,
        int b2,
        int b3,
        int b4,
        int b5,
        int b6,
        int b7,
        int b8,
        int b9,
        int b10,
        int b11,
        bool button_lock
    );
    
    void valueChanged(const std::string& name, mopo::mopo_float value);
    void valueChangedThroughMidi(const std::string& name, mopo::mopo_float value) override;
    void patchChangedThroughMidi(juce::File patch) override;
    void valueChangedExternal(const std::string& name, mopo::mopo_float value);
    void valueChangedInternal(const std::string& name, mopo::mopo_float value);
    void changeModulationAmount(const std::string& source, const std::string& destination,
                               mopo::mopo_float amount);
    void setModulationAmount(mopo::ModulationConnection* connection, mopo::mopo_float amount);
    void disconnectModulation(mopo::ModulationConnection* connection);
    void clearModulations();
    int getNumModulations(const std::string& destination);
    std::set<mopo::ModulationConnection*> getModulationConnections() { return mod_connections_; }
    std::vector<mopo::ModulationConnection*> getSourceConnections(const std::string& source);
    std::vector<mopo::ModulationConnection*> getDestinationConnections(
        const std::string& destination);
  
    mopo::Output* getModSource(const std::string& name);

    void loadInitPatch();
    bool loadFromFile(juce::File patch);
    bool exportToFile();
    bool saveToFile(juce::File patch);
    bool saveToActiveFile();
    juce::File getActiveFile() { return active_file_; }

    virtual void beginChangeGesture(const std::string& name) { }
    virtual void endChangeGesture(const std::string& name) { }
    virtual void setValueNotifyHost(const std::string& name, mopo::mopo_float value) { }

    void armMidiLearn(const std::string& name);
    void cancelMidiLearn();
    void clearMidiLearn(const std::string& name);
    bool isMidiMapped(const std::string& name);

    void setAuthor(juce::String author);
    void setPatchName(juce::String patch_name);
    void setFolderName(juce::String folder_name);
    juce::String getAuthor();
    juce::String getPatchName();
    juce::String getFolderName();

    mopo::control_map& getControls() { return controls_; }
    mopo::HelmEngine* getEngine() { return &engine_; }
    juce::MidiKeyboardState* getKeyboardState() { return keyboard_state_; }
    const float* getOutputMemory() { return output_memory_; }
    mopo::ModulationConnectionBank& getModulationBank() { return modulation_bank_; }

    struct ValueChangedCallback : public juce::CallbackMessage {
      ValueChangedCallback(SynthBase* listener, std::string name, mopo::mopo_float val) :
          listener(listener), control_name(name), value(val) { }

      void messageCallback() override;

      SynthBase* listener;
      std::string control_name;
      mopo::mopo_float value;
    };

  protected:
    virtual const juce::CriticalSection& getCriticalSection() = 0;
    virtual SynthGuiInterface* getGuiInterface() = 0;
    juce::var saveToVar(juce::String author);
    void loadFromVar(juce::var state);
    mopo::ModulationConnection* getConnection(const std::string& source,
                                              const std::string& destination);

    inline bool getNextControlChange(mopo::control_change& change) {
      return value_change_queue_.try_dequeue(change);
    }

    inline bool getNextModulationChange(mopo::modulation_change& change) {
      return modulation_change_queue_.try_dequeue(change);
    }

    void processAudio(juce::AudioSampleBuffer* buffer, int channels, int samples, int offset);
    void processMidi(juce::MidiBuffer& buffer, int start_sample = 0, int end_sample = 0);
    void processKeyboardEvents(juce::MidiBuffer& buffer, int num_samples);
    void processControlChanges();
    void processModulationChanges();
    void updateMemoryOutput(int samples, const mopo::mopo_float* left,
                                         const mopo::mopo_float* right);

    mopo::ModulationConnectionBank modulation_bank_;
    mopo::HelmEngine engine_;
    juce::ScopedPointer<MidiManager> midi_manager_;
    juce::ScopedPointer<juce::MidiKeyboardState> keyboard_state_;

    juce::File active_file_;
    float output_memory_[2 * mopo::MEMORY_RESOLUTION];
    float output_memory_write_[2 * mopo::MEMORY_RESOLUTION];
    mopo::mopo_float last_played_note_;
    int last_num_pressed_;
    mopo::mopo_float memory_reset_period_;
    mopo::mopo_float memory_input_offset_;
    int memory_index_;

    std::map<std::string, juce::String> save_info_;
    mopo::control_map controls_;
    std::set<mopo::ModulationConnection*> mod_connections_;
    moodycamel::ConcurrentQueue<mopo::control_change> value_change_queue_;
    moodycamel::ConcurrentQueue<mopo::modulation_change> modulation_change_queue_;

    std::map<std::string, int> gamepad_axis_mapping_;
    std::map<std::string, int> gamepad_btn_mapping_;
};

#endif // SYNTH_BASE_H
